﻿var velocidad:float=5;
var direccion:Vector3;
var colision:boolean;
var trigger:boolean;
var sonido:boolean;
var archivoSonido:AudioClip;
var efectoDisparo:boolean;
var archivoEfecto:Transform;
var destruir:boolean;
///////////////////////////////
//static var Impacto:boolean;
function Start () {

}

function Update () {
transform.Translate(direccion*Time.deltaTime*velocidad);
}

function OnCollisionEnter (collision : Collision) {
	if(colision==true)
		{
		//Impacto=true;
		if(sonido==true)
			{
			audio.PlayClipAtPoint(archivoSonido,transform.position);
			}
		if(efectoDisparo==true)
			{
			Instantiate(archivoEfecto, transform.position,transform.rotation);
			}
		if(destruir==true)
			{
			Destroy (gameObject);
			}
		}
}
function OnTriggerEnter (other : Collider) {
	if(trigger==true)
		{
		//Impacto=true;
		if(sonido==true)
			{
			audio.PlayClipAtPoint(archivoSonido,transform.position);
			}
		if(efectoDisparo==true)
			{
			Instantiate(archivoEfecto, transform.position,transform.rotation);
			}
		if(destruir==true)
			{
			Destroy (gameObject);
			}	
		}
}
@script RequireComponent(Rigidbody)