var HayRotacion : boolean;
var anguloRotacion:Vector3;
var RotacionEstatica : boolean;
var RotacionIdaVuelta : boolean;
var anguloRotacionNegativo:Vector3;
var TiempoAEsperar : float;
var TiempoMinRotacion : float;
var TiempoMaxRotacion : float;

private var TiempoEstRotacion : float;
private var i:int=0;
private var lado1 : boolean;
//rotacionactiva era static
private var rotacionactiva : boolean;


function Start(){
	if (HayRotacion == true){
		rotacionactiva = true;
	}
	if (RotacionIdaVuelta == true){
		lado1 = true;
	}
}

function Update(){
	if (rotacionactiva == true){
		if (RotacionEstatica == true){
			if (Time.time > TiempoEstRotacion){
				TiempoEstRotacion = Time.time + TiempoAEsperar;
				if (RotacionIdaVuelta == true){
					if (lado1 == true){
						transform.Rotate(anguloRotacion);
						lado1 = false;
					}
					else if (lado1 == false){
						transform.Rotate(anguloRotacionNegativo);
						lado1 = true;
					}
				}
				if (RotacionIdaVuelta == false){
					transform.Rotate(anguloRotacion);
				}
			}
		}
		if (RotacionEstatica == false){
			var tiempo = Random.Range(TiempoMinRotacion,TiempoMaxRotacion);
			if (Time.time > TiempoEstRotacion){
				TiempoEstRotacion = Time.time + tiempo;
				if (RotacionIdaVuelta == true){
					if (lado1 == true){
						transform.Rotate(anguloRotacion);
						lado1 = false;
					}
					else if (lado1 == false){
						transform.Rotate(anguloRotacionNegativo);
						lado1 = true;
					}
				}
				if (RotacionIdaVuelta == false){
					transform.Rotate(anguloRotacion);
				}
			}
		}
	}
}

@script RequireComponent(GameObject)